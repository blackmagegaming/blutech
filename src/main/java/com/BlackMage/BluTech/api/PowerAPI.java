package com.BlackMage.BluTech.api;

import com.BlackMage.BluTech.Blocks.Wire.BasicWire;

public class PowerAPI {

	public static double PowerThru;
	
	
	public void CanProvidePower(){	
		
	}
	
	public void NeedsPower(){
		
	}
	
	//Determines the amount of power traveling thru the wires
	//requires an input of conductivity, thickness resistance and distance
	public static double ElectricWire(BasicWire BaseWire){
		
		PowerThru = ((BaseWire.getConductivity() * BaseWire.getThickness()) - (BaseWire.getResistance() * BaseWire.getDistance()));
		if (PowerThru < 0){
			PowerThru = 0;
		}
		return PowerThru;
	}
	
	public void Converter(){
		
	}

}
