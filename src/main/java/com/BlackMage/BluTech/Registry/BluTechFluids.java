package com.BlackMage.BluTech.Registry;

import net.minecraftforge.fluids.Fluid;

/*
 * A class to manage and register all kinds of Fluids used by BluTech.
 */
public class BluTechFluids {

	/*
	 *  Fluid Fields - If you are adding a *Fluid* then you need to and and create a public static final field in this class.
	 *                 Please note that if you are adding a *Liquid* (like water blocks, lava blocks, liquid hydrogen, something that
	 *                 inherits from BlockFluidClassic, you MUST create the fluid reference here (if need be) and then go to BluTechLiquids
	 *                 (in the case of liquids) and add the liquid field there. This is a result of Fluids needing to be created and registered
	 *                 before Liquids (BlockFluidClassic reference) can be registered.
	 *                 
	 *                 Also, at the time of writing, you will need to reset the fluid's unlocalized name to that of the liquid's name. We should
	 *                 work to fix that (and it should be after the naming conventions are laid out).
	 *                 
	 * PLEASE try to keep field names in alphabetical order!
	 */
	public static final Fluid fluidHydrogen = new Fluid("fluidHydrogen").setViscosity(3000).setDensity(7000);
	
	
	/*
	 * Registering fluids - IMPORTANT: If you are adding a new Fluid object to the registry, MAKE SURE that it is registered BEFORE
	 *                      the call to "BluTechLiquids.registerLiquids()". Failure to do so, will result in an automatic crash since
	 *                      Minecraft requires Fluids to be registered before it can register the block that the fluid is related to.
	 *                      
	 *                      To add a fluid, make a call to RegisterHelper.registerFluid(yourFluid), where your fluid is the fluid that you're adding.
	 *                      Once that is done, apparently you will need to change the unlocalized name to the unlocalized name of the 
	 *                      liquid that the Fluid will be registered to. (See below for current implementation.
	 *                      
	 * PLEASE try to keep these in alphabetical order by the name of the fluid being added!
	 */
	public static void registerFluids() {
		// Register all fluids to the game
		RegisterHelper.registerFluid(fluidHydrogen);
		
		// Register all liquids to the game
		BluTechLiquids.registerLiquids();
		
		// Update the unlocalized name
		// FIXME: There needs to be a better way to do this - new naming convention could probably eliminate this
		fluidHydrogen.setUnlocalizedName(BluTechLiquids.liquidHydrogen.getUnlocalizedName());
		
		// Register any other fluid-based blocks into the game!
	}
}
