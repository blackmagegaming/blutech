package com.BlackMage.BluTech.Registry;

import net.minecraftforge.fml.common.Mod.Instance;

import com.BlackMage.BluTech.BluTech;

/*
 * A class containing the logic to register all of BluTech's blocks, items, etc to Minecraft.
 */
public final class BluTechRegistry {
	
	@Instance(BluTech.MODID)
	public static BluTech modInstance;

	// A flag which signifies that everything has been registered.
	private static boolean registered = false;
	
	
	/*
	 * Register logic for registering all of BluTech's blocks, items, etc to Minecraft.
	 */
	public static final void register() {
		// Make sure that someone didn't already make this call!
		// FIXME: Should we use assert() instead?
		if(registered) return;
		
		
		// Register Blocks
		BluTechBlocks.registerBlocks();
		
		// Register Fluids
		BluTechFluids.registerFluids();
		
		// Register Gases
		BluTechGases.registerGases();
		
		//Register Battle Gear
		BluTechItemBattles.registerBattles();
		
		// Register Items
		BluTechItemMaterials.registerItemMaterials();
		
		// Register Tools
		BluTechItemTools.registerTools();
		
		// Register Biomes
		BluTechBiomes.registerBiomes();
		
    	BluTechAchievements.registerAchievements();
       	
    	//BiomeDictionary.registerAllBiomes();
    	
    	BluTechRecipes.registerRecipes();
    	
    	// Set the registered flag, so no calls to this method can EVER happen again during the current execution.
    	registered = true;
	}
}
