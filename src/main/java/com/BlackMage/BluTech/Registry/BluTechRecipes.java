package com.BlackMage.BluTech.Registry;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BluTechRecipes {

	// Register all of the recipes in BluTech
	public static void registerRecipes() {
		
		GameRegistry.addRecipe(new ItemStack(BluTechBlocks.bluStoneBlock,1), new Object[]{
			"TTT","TTT","TTT",'T',BluTechItemMaterials.bluStoneIngot,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStonePickaxe), new Object[]{
            "XXX"," S "," S ", 'X', BluTechItemMaterials.bluStoneIngot, 'S', Items.stick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneHoe), new Object[]{
            "XX "," S "," S ", 'X', BluTechItemMaterials.bluStoneIngot, 'S', Items.stick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneAxe), new Object[]{
            "XX ","XS "," S ", 'X', BluTechItemMaterials.bluStoneIngot, 'S', Items.stick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneShovel), new Object[]{
            " X "," S "," S ", 'X', BluTechItemMaterials.bluStoneIngot, 'S', Items.stick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.bluStoneChunk), new Object[]{
            "BB ","BB ", 'B', BluTechItemMaterials.bluStoneDust,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.bluStoneShard), new Object[]{
			"L  "," C ","  L", 'C', BluTechItemMaterials.bluStoneChunk, 'L', new ItemStack(Items.dye, 1, 4),
		});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.bluStoneCrystal), new Object[]{
			"sss","sss","sss", 's', BluTechItemMaterials.bluStoneShard,
		});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.bluStoneGem), new Object[]{
			"ccc","cdc","ccc", 'c', BluTechItemMaterials.bluStoneCrystal, 'd', Items.diamond
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneGemPickaxe), new Object[]{
			"XXX"," S "," S ", 'X', BluTechItemMaterials.bluStoneGem, 'S', BluTechItemMaterials.infusedStick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneGemHoe), new Object[]{
			"XX "," S "," S ", 'X', BluTechItemMaterials.bluStoneGem, 'S', BluTechItemMaterials.infusedStick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneGemAxe), new Object[]{
			"XX ","XS "," S ", 'X', BluTechItemMaterials.bluStoneGem, 'S', BluTechItemMaterials.infusedStick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemTools.bluStoneGemShovel), new Object[]{
			" X "," S "," S ", 'X', BluTechItemMaterials.bluStoneGem, 'S', BluTechItemMaterials.infusedStick,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.infusedStick), new Object[]{
			" s "," b ", 's', Items.stick, 'b', BluTechItemMaterials.bluStoneDust,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.reinforcedBluStoneGem), new Object[]{
			"o o"," g ", "o o", 'o', Blocks.obsidian, 'g', BluTechItemMaterials.bluStoneGem,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemMaterials.singularityCore), new Object[]{
			"ogo","gdg", "ogo", 'o', Blocks.obsidian, 'g', BluTechItemMaterials.bluStoneGem, 'd', Items.diamond,
			});
		/*GameRegistry.addRecipe(new ItemStack(BluTechItemBattles.BlustoneHelmet), new Object[]{
			"rsr","r r", "   ", 'r', ReinforcedBluStoneGem, 's', SingularityCore,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemBattles.BlustoneBoots), new Object[]{
			"   ","rsr", "r r", 'r', ReinforcedBluStoneGem, 's', SingularityCore,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemBattles.BlustoneLeggings), new Object[]{
			"rsr","r r", "r r", 'r', ReinforcedBluStoneGem, 's', SingularityCore,
			});
		GameRegistry.addRecipe(new ItemStack(BluTechItemBattles.BlustoneChestplate), new Object[]{
			"r r","rsr", "rrr", 'r', ReinforcedBluStoneGem, 's', SingularityCore,
			});*/
		
		//Shapeless Crafting
		GameRegistry.addShapelessRecipe(new ItemStack(BluTechItemMaterials.bluStoneDust,9), new Object[]{
			BluTechBlocks.bluStoneBlock });

		GameRegistry.addShapelessRecipe(new ItemStack(BluTechItemMaterials.bluStoneDust,4), new Object[]{
			BluTechItemMaterials.bluStoneChunk });
		
	}
}
