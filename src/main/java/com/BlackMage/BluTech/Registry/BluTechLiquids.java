package com.BlackMage.BluTech.Registry;

import com.BlackMage.BluTech.Liquids.LiquidHydrogen;

public class BluTechLiquids {

	/*
	 * Liquid Fields - If you have read the other registry files, this should seem pretty straightforward. Create a
	 *                 public static final reference to whatever liquid you are adding to this class.
	 *                 
	 * PLEASE try to keep this names in alphabetical order!
	 */
	public static final LiquidHydrogen liquidHydrogen = new LiquidHydrogen();
	
	
	// DO NOT CALL THIS METHOD FROM OUTSIDE OF BluTechFluids! IT *WILL* CAUSE CRASHES!
	/*
	 * Registering Liquids - If you are registering liquids, then you will need to just register the block. Make a call to
	 *                       RegisterHelper.registerBlock(yourLiquid) with the liquid that you want to register. See the current
	 *                       implmentation if you are having problems.
	 *                       
	 * PLEASE try to keep these calls in order by the name of the liquid being added.
	 */
	public static void registerLiquids() {
		
		//RegisterHelper.registerBlock(liquidHydrogen);
	}
}
