package com.BlackMage.BluTech.Registry;

import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.common.BiomeManager;

import com.BlackMage.BluTech.Biome.BiomeGenArtic;

/*
 * A class that can manage and register BluTech Biomes to Minecraft.
 * NOTE: This is a work in progress and should not be used
 */
public class BluTechBiomes {
	/*
	 * Biome fields - If you are going to add a biome, then you need to make sure this class has a public static final 
	 *                field of the same type of the biome being added to the game.
	 *                
	 * Example      - If you are adding a new biome called "Artic", the you need to create a static field like the following:
	 * 
	 *                    public static final BiomeGenBase biomeArtic = new BiomeGenArtic(...).setBioName("Artic"); 
	 *                
	 *                See below for the current implementation if you are having problems.
	 *                
	 * PLEASE try to keep field names in alphabetical order!
	 */
	public static final BiomeGenBase biomeArtic = new BiomeGenArtic(137).setBiomeName("Artic");
	
	
	/*
	 * TODO: Registering Biomes Description
	 */
	public static void registerBiomes(){
		BiomeDictionary.registerBiomeType(biomeArtic, Type.SNOWY);
		BiomeManager.addSpawnBiome(biomeArtic);
		
	}

}

