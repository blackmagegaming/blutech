package com.BlackMage.BluTech.Registry;

import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;

/*
 * A class that can manage and register BluTech Achievements to Minecraft
 */
public class BluTechAchievements {		
	/*
	 * Achievement fields - If you are going to add a block, then you need to make sure this class has a public static final 
	 *                      field of the Achievement to be added.
	 *                
	 * Example            - If you are adding an achievement, then you need to create a static field like the following
	 * 
	 *                          public static final Achievement aquireBluStone = new Achievement(...);
	 *                
	 *                      See below for the current implementation if you are having problems.
	 *                      
	 * PLEASE try to keep Achievements in alphabetical order by field name!
	 */
	// FIXME: Achievements aren't working quite right (textures), it's probably in the constructors
	public static final Achievement aquireBluStone  =  new Achievement("achievement.AquireBluStone", "AquireBluStone", 0, 0, BluTechItemMaterials.bluStoneDust, null)/*.setIndependent()*/;

	public static final Achievement aquireCarbon    =  new Achievement("achievement.AquireCarbon",   "AquireCarbon",  -2, 0, BluTechItemMaterials.carbonChunk,  null)/*.setIndependent()*/;

	public static final Achievement aquireLithium   =  new Achievement("achievement.AquireLithium",  "AquireLithium",  2, 0, BluTechItemMaterials.lithiumChunk, null)/*.setIndependent()*/;

	public static final Achievement craftHammer     =  new Achievement("achievement.CraftHammer",    "CraftHammer",    1, 2, BluTechItemMaterials.bluStoneDust, null)/*.setIndependent()*/;
	
	
	/*
	 * Registering Achievements - If you are adding an achievement, then you will need to make sure that this function registers it into
	 *                            the game. For ease of use, the RegisterHelper class comes in real handy for this operation.
	 *                            All you have to do is add the static field of the achievement that you created, to the array
	 *                            being passed into the registerAchievements method in RegisterHelper.
	 *                            
	 * PLEASE try to keep additions in alphabetical order by field name!
	 */
	public static void registerAchievements() {

		RegisterHelper.registerAchievements(
			new Achievement[] {
					aquireBluStone,
					aquireCarbon,
					aquireLithium,
					craftHammer
		});
	}
}
