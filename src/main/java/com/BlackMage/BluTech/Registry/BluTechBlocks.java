package com.BlackMage.BluTech.Registry;

import com.BlackMage.BluTech.Blocks.BluStoneBlock;
import com.BlackMage.BluTech.Blocks.BluStoneOre;
import com.BlackMage.BluTech.Blocks.CarbonBlock;
import com.BlackMage.BluTech.Blocks.CarbonOre;
import com.BlackMage.BluTech.Blocks.CrackedMossyStoneBrick;
import com.BlackMage.BluTech.Blocks.GrassyCrackedMossyStoneBrick;
import com.BlackMage.BluTech.Blocks.GrassyCrackedStoneBrick;
import com.BlackMage.BluTech.Blocks.GrassyMossyStoneBrick;
import com.BlackMage.BluTech.Blocks.LightningRod;
import com.BlackMage.BluTech.Blocks.LithiumBlock;
import com.BlackMage.BluTech.Blocks.LithiumOre;
import com.BlackMage.BluTech.Blocks.ObsidianFurnace;


/*
 * A class to easily manage and register BluTech blocks within Minecraft
 */
public class BluTechBlocks {
	/*
	 * Block fields - If you are going to add a block, then you need to make sure this class has a public static final 
	 *                field of the same type of the block being added to the game.
	 *                
	 * Example      - If you are adding the CarbonOre block, the you need to create a static field like the following
	 * 
	 *                public static final CarbonOre carbonOre = new CarbonOre(); 
	 *                
	 *                See below for more examples if you are having problems.
	 *                
	 * PLEASE try to keep these fields in alphabetical order!
	 */
	public static final BluStoneBlock   bluStoneBlock  =  new BluStoneBlock();
	
	public static final BluStoneOre     bluStoneOre    =  new BluStoneOre();
	
	public static final CarbonBlock       carbonBlock      =  new CarbonBlock();
	
	public static final CarbonOre       carbonOre      =  new CarbonOre();
	
	public static final CrackedMossyStoneBrick crackedMossyStoneBrick = new  CrackedMossyStoneBrick();
	
	public static final GrassyCrackedStoneBrick grassyCrackedStoneBrick = new  GrassyCrackedStoneBrick();
	
	public static final GrassyMossyStoneBrick grassyMossyStoneBrick = new  GrassyMossyStoneBrick();
	
	public static final GrassyCrackedMossyStoneBrick grassyCrackedMossyStoneBrick = new  GrassyCrackedMossyStoneBrick();
	
	public static final LightningRod lightningRod = new LightningRod();
	
	public static final LithiumBlock      lithiumBlock     =  new LithiumBlock();
	
	public static final LithiumOre      lithiumOre     =  new LithiumOre();
	
	public static final ObsidianFurnace	obsidianFurnace	= new ObsidianFurnace(false);
	
	public static final ObsidianFurnace	obsidianFurnaceActive	= new ObsidianFurnace(true);
	
	
	/*
	 * Registering blocks - If you are adding a block, then you will need to make sure that this function registers it into
	 *                      the game. For ease of use, the RegisterHelper class comes in real handy for this operation.
	 *                      Use that class in this function to register blocks. Also, make sure that the block you are
	 *                      creating sets an unlocalized name, otherwise you will probably run into problems registering
	 *                      the block. See the current implementation below if you are having problems.
	 *                      
	 * PLEASE try to keep new additions in alphabetical order by field name!
	 */
	public static void registerBlocks() {
		
		// Now  all the below blocks will be registered into the game!
		RegisterHelper.registerBlock(bluStoneBlock);
		RegisterHelper.registerBlock(bluStoneOre);
		RegisterHelper.registerBlock(carbonBlock);
		RegisterHelper.registerBlock(carbonOre);
		RegisterHelper.registerBlock(crackedMossyStoneBrick);
		RegisterHelper.registerBlock(grassyMossyStoneBrick);
		RegisterHelper.registerBlock(grassyCrackedStoneBrick);
		RegisterHelper.registerBlock(grassyCrackedMossyStoneBrick);
		RegisterHelper.registerBlock(lightningRod);
		RegisterHelper.registerBlock(lithiumBlock);
		RegisterHelper.registerBlock(lithiumOre);
		RegisterHelper.registerBlock(obsidianFurnace);
		RegisterHelper.registerBlock(obsidianFurnaceActive);
		
	}
}
