package com.BlackMage.BluTech.Registry;

import com.BlackMage.BluTech.Gases.HydrogenGas;
import com.BlackMage.BluTech.Gases.MethaneGas;

public class BluTechGases {

	/*
	 * Gas fields - If you are going to add a gas, then you need to make sure this class has a public static final 
	 *              field of the same type of the gas being added to the game.
	 *                
	 * Example    - If you are adding the HydrogenGas gas, the you need to create a static field like the following
	 * 
	 *                  public static final HydrogenGas hydrogenGas = new HydrogenGas(); 
	 *                
	 *              See below for current implementation if you are having problems.
	 *              
	 *              
	 * PLEASE try to keep these in alphabetical order!
	 */
	public static final HydrogenGas hydrogenGas = new HydrogenGas();
	public static final MethaneGas MethaneGas = new MethaneGas();
	
	
	/*
	 * Registering Gases - If you are adding a new gas, then you will need to register the gas to Minecraft. Thankfully,
	 *                     the RegisterHelper class comes in handy for this operation. To register the gas, call the appropriate
	 *                     method in RegisterHelper.
	 *            
	 * Example           - Say you wanted to register the hydrogenGas gas field that you created above, then you should add 
	 *                     the following to the function below:
	 *                         
	 *                         RegisterHelper.registerBlock(hydrogenGas); // Notice that HydrogenGas is actually a subclass of Block!
	 *                     
	 *                     If you are having trouble take a look at the current implementation for some help.
	 *                     
	 * PLEASE try to keep new additions in alphabetical order based on the GAS's name!
	 */
	public static void registerGases() {
		/*
		RegisterHelper.registerBlock(hydrogenGas);
		RegisterHelper.registerBlock(MethaneGas); */
	}
}
