package com.BlackMage.BluTech.Registry;

import com.BlackMage.BluTech.Items.Tools.BluStoneAxe;
import com.BlackMage.BluTech.Items.Tools.BluStoneGemAxe;
import com.BlackMage.BluTech.Items.Tools.BluStoneGemHoe;
import com.BlackMage.BluTech.Items.Tools.BluStoneGemPickaxe;
import com.BlackMage.BluTech.Items.Tools.BluStoneGemShovel;
import com.BlackMage.BluTech.Items.Tools.BluStoneHoe;
import com.BlackMage.BluTech.Items.Tools.BluStonePickaxe;
import com.BlackMage.BluTech.Items.Tools.BluStoneShovel;
import com.BlackMage.BluTech.Items.Tools.Hammer;
import com.BlackMage.BluTech.Items.Tools.Sift;

public class BluTechItemTools {
	/*
	 * Item-Tool fields - If you are going to add an Item-Tool, then you need to make sure this class has a public static final 
	 *                    field of the same type of the Item-Tool being added to the game.
	 *                
	 * Example          - If you are adding the Hammer Item-Tool, the you need to create a static field like the following
	 * 
	 *                        public static final HydrogenGas hydrogenGas = new HydrogenGas(); 
	 *                
	 *                    See below for current implementation if you are having problems.
	 *              
	 *              
	 * PLEASE try to keep these in alphabetical order!
	 */	
	public static final BluStoneAxe  bluStoneAxe  = new BluStoneAxe();
	
	public static final BluStoneGemAxe  bluStoneGemAxe  = new BluStoneGemAxe();
	
	public static final BluStoneGemHoe  bluStoneGemHoe  = new BluStoneGemHoe();
	
	public static final BluStoneGemPickaxe  bluStoneGemPickaxe  = new BluStoneGemPickaxe();
	
	public static final BluStoneGemShovel  bluStoneGemShovel  = new BluStoneGemShovel();
	
	public static final BluStoneHoe  bluStoneHoe  = new BluStoneHoe();
	
	public static final BluStonePickaxe  bluStonePickaxe  = new BluStonePickaxe();
	
	public static final BluStoneShovel  bluStoneShovel = new BluStoneShovel();
	
	public static final Hammer  hammer  = new Hammer();
	
	public static final Sift    sift    = new Sift();
	
	
	/*
	 * Registering Item-Tools - If you are adding a new Item-Tool, then you will need to register the gas to Minecraft. Thankfully,
	 *                          the RegisterHelper class comes in handy for this operation. To register the gas, call the appropriate
	 *                          method in RegisterHelper.
	 *            
	 * Example                - Say you wanted to register the hydrogenGas gas field that you created above, then you should add 
	 *                          the following to the function below:
	 *                         
	 *                              RegisterHelper.registerBlock(hydrogenGas); // Notice that HydrogenGas is actually a subclass of Block!
	 *                     
	 *                          If you are having trouble take a look at the current implementation for some help.
	 *                     
	 * PLEASE try to keep new additions in alphabetical order based on the field's name!
	 */
	public static void registerTools() {
		
		RegisterHelper.registerItem(bluStoneAxe);
		RegisterHelper.registerItem(bluStoneGemAxe);
		RegisterHelper.registerItem(bluStoneGemHoe);
		RegisterHelper.registerItem(bluStoneGemPickaxe);
		RegisterHelper.registerItem(bluStoneGemShovel);
		RegisterHelper.registerItem(bluStoneHoe);
		RegisterHelper.registerItem(bluStonePickaxe);
		RegisterHelper.registerItem(bluStoneShovel);
		RegisterHelper.registerItem(hammer);
		RegisterHelper.registerItem(sift);
	}
}
