package com.BlackMage.BluTech.Registry;

// BluTech Imports 
import com.BlackMage.BluTech.Items.Materials.BluStoneChunk;
import com.BlackMage.BluTech.Items.Materials.BluStoneCrystal;
import com.BlackMage.BluTech.Items.Materials.BluStoneDust;
import com.BlackMage.BluTech.Items.Materials.BluStoneGem;
import com.BlackMage.BluTech.Items.Materials.BluStoneIngot;
import com.BlackMage.BluTech.Items.Materials.BluStoneShard;
import com.BlackMage.BluTech.Items.Materials.BucketHydrogen;
import com.BlackMage.BluTech.Items.Materials.CarbonChunk;
import com.BlackMage.BluTech.Items.Materials.Changelog;
import com.BlackMage.BluTech.Items.Materials.InfusedStick;
import com.BlackMage.BluTech.Items.Materials.LithiumChunk;
import com.BlackMage.BluTech.Items.Materials.ReinforcedBluStoneGem;
import com.BlackMage.BluTech.Items.Materials.SingularityCore;

/*
 * A class which can manage and register BluTech Item-Materials to Minecraft.
 */
public class BluTechItemMaterials {
	/*
	 * Item-Material fields - If you are going to add an Item-Material, then you need to make sure this class has a public static final 
	 *                        field of the same type of the Item-Material being added to the game.
	 *                
	 * Example              - If you are adding the BluStoneDust Item-Material, then you need to create a static field like the following
	 * 
	 *                            public static final BluStoneDust bluStoneDust = new BluStoneDust(); 
	 *                
	 *                        See below for the current implementation if you are having problems.
	 *                
	 * PLEASE try to keep these in alphabetical order by field name!
	 */
	public static final BluStoneChunk    bluStoneChunk    = new BluStoneChunk();
	
	public static final BluStoneCrystal    bluStoneCrystal    = new BluStoneCrystal();
	
	public static final BluStoneDust    bluStoneDust    = new BluStoneDust();
	
	public static final BluStoneGem    bluStoneGem    = new BluStoneGem();
	
	public static final BluStoneIngot bluStoneIngot = new BluStoneIngot();
	
	public static final BluStoneShard    bluStoneShard    = new BluStoneShard();
	
	public static final BucketHydrogen  bucketHydrogen  = new BucketHydrogen(BluTechLiquids.liquidHydrogen);
	
	public static final CarbonChunk     carbonChunk     = new CarbonChunk();
	
	public static final Changelog       changelog       = new Changelog();
	
	public static final InfusedStick    infusedStick    = new InfusedStick();
	
	public static final LithiumChunk    lithiumChunk    = new LithiumChunk();
	
	public static final ReinforcedBluStoneGem    reinforcedBluStoneGem    = new ReinforcedBluStoneGem();
	
	public static final SingularityCore    singularityCore    = new SingularityCore();
	
	
	/*
	 * Registering ItemMaterials - If you are adding a new Item-Material, then you need to register it to Minecraft. Thankfully, the 
	 *                             RegisterHelper class comes in real handy for this operation. Use the appropriate method in RegisterHelper 
	 *                             to register the Item-Material to Minecraft
	 *                             
	 * Example                   - If you are adding the bluStoneDust object that was created in the example above you would use the following
	 *                             to register the Item-Material to Minecraft:
	 *                             
	 *                                 RegisterHelper.registerItem(bluStoneDust);
	 *                                 
	 *                             See below for the current implementation if you are having problems.
	 *                             
	 * PLEASE try to keep new additions in alphabetical order by the field name!
	 */
	public static final void registerItemMaterials() {
		
		RegisterHelper.registerItem(bluStoneChunk);
		RegisterHelper.registerItem(bluStoneCrystal);
		RegisterHelper.registerItem(bluStoneDust);
		RegisterHelper.registerItem(bluStoneGem);
		RegisterHelper.registerItem(bluStoneIngot);
		RegisterHelper.registerItem(bluStoneShard);
		RegisterHelper.registerItem(bucketHydrogen);
		RegisterHelper.registerItem(carbonChunk);
		RegisterHelper.registerItem(changelog);
		RegisterHelper.registerItem(infusedStick);
		RegisterHelper.registerItem(lithiumChunk);
		RegisterHelper.registerItem(reinforcedBluStoneGem);
		RegisterHelper.registerItem(singularityCore);
				
		registerBuckets();
	}
	
	/*
	 * Additional logic to add buckets to Minecraft.
	 */
	public static void registerBuckets() {
		RegisterHelper.registerBucket(bucketHydrogen, BluTechLiquids.liquidHydrogen);
	}
}
