package com.BlackMage.BluTech.Registry;

import com.BlackMage.BluTech.Items.Battles.BluStoneGemSword;
import com.BlackMage.BluTech.Items.Battles.BluStoneSword;


public class BluTechItemBattles {
	/*
	 * Item-Tool fields - If you are going to add an Item-Tool, then you need to make sure this class has a public static final 
	 *                    field of the same type of the Item-Tool being added to the game.
	 *                
	 * Example          - If you are adding the Hammer Item-Tool, the you need to create a static field like the following
	 * 
	 *                        public static final HydrogenGas hydrogenGas = new HydrogenGas(); 
	 *                
	 *                    See below for current implementation if you are having problems.
	 *              
	 *              
	 * PLEASE try to keep these in alphabetical order!
	 */
	public static final BluStoneGemSword bluStoneGemSword = new BluStoneGemSword();
	
	public static final BluStoneSword bluStoneSword = new BluStoneSword();
	
	
	/*
	 * Registering Item-Tools - If you are adding a new Item-Tool, then you will need to register the gas to Minecraft. Thankfully,
	 *                          the RegisterHelper class comes in handy for this operation. To register the gas, call the appropriate
	 *                          method in RegisterHelper.
	 *            
	 * Example                - Say you wanted to register the hydrogenGas gas field that you created above, then you should add 
	 *                          the following to the function below:
	 *                         
	 *                              RegisterHelper.registerBlock(hydrogenGas); // Notice that HydrogenGas is actually a subclass of Block!
	 *                     
	 *                          If you are having trouble take a look at the current implementation for some help.
	 *                     
	 * PLEASE try to keep new additions in alphabetical order based on the field's name!
	 */
	public static void registerBattles() {
		
		RegisterHelper.registerItem(bluStoneGemSword);
		
		RegisterHelper.registerItem(bluStoneSword);
		
	}
}