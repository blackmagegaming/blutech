package com.BlackMage.BluTech.Registry;

// Minecraft imports
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBucket;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.Blocks.BluTechBlock;
import com.BlackMage.BluTech.EventHandlers.BluTechHandler;
//Forge imports
//BluTech imports

/*
 *  A class to help us register the stuff in our mod to the game.
 *  Basically, we can use the static methods in this class to easily register blocks, items, etc from our mod
 *  into Minecraft, without making the code look weird. It also will save some typing, and make upgrades easier.
 *  Also keep in mind that we should be prepending BluTech's MODID when registering something to the game, to avoid
 *  conflicts with other mods.
 */
public class RegisterHelper {
	
	/*
	 * This function takes any kind of block, and adds it to the game registry along with BluTech's MODID.
	 */
	public static void registerBlock(BluTechBlock block) {
		try {
			Block b = (Block) block;
			// Set the unlocalized name
			b.setUnlocalizedName(BluTech.MODID + "_" + block.getName());
			
			// Set the creative tabs
			b.setCreativeTab(block.getCreativeTab());
			
			// TODO: Register the block
			GameRegistry.registerBlock(b, block.getName());
			Item i = Item.getItemFromBlock(b);
			System.out.println("Name is " + block.getName());
			Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(i, 0, new ModelResourceLocation(BluTech.MODID + ":" + block.getName(), "inventory"));
		} catch (Exception e){
			System.out.println(block.getName() + " was not registered correctly!");
		}
	}
	
	/*
	 * This function takes any kind of GUI, and adds it to the registry
	 */
	
	public static void registerGui(){
	}

	
	/*
	 * This function takes any kind of item, and adds it to the game registry along with BluTech's MODID.
	 */
	public static void registerItem(Item item) {
		try {
			Item i = (Item) item;
			
			// Set the unlocalized name
			//i.setUnlocalizedName(BluTech.MODID + "_" + item.getName());
			
			// TODO: Any additional item logic
			
			// Register Item
			GameRegistry.registerItem(item, item.getUnlocalizedName());
			Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(BluTech.MODID + "_" + item.getUnlocalizedName(), "inventory"));
		} catch (Exception e) {
			//System.out.println(item.getName() + " wasn't registered correctly!");
		}
	}
	
	/*
	 * This function takes an array of Achievements and registers them to Minecraft in a new Achievement page
	 */
	public static void registerAchievements(Achievement[] achievements) {
		AchievementPage.registerAchievementPage(new AchievementPage("BluTech Achievements", achievements));
	}
	
	/*
	 * This function takes a fluid and registers it with minecraft.
	 */
	public static void registerFluid(Fluid fluid) {
		FluidRegistry.registerFluid(fluid);
	}
	
	/*
	 * This function takes an ItemBucket and the liquid Block and registers it with Minecraft.
	 */
	public static void registerBucket(ItemBucket bucket, BlockFluidClassic liquid) {
		BluTechHandler.INSTANCE.buckets.put(liquid, bucket);
		
		// FIXME: Is this next line necessary anymore?
		//FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(liquid.getFluid().getName(), FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(bucket), new ItemStack(Items.bucket));
	}
}
