package com.BlackMage.BluTech.World;

import java.util.List;

import net.minecraftforge.common.BiomeManager.BiomeType;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class WorldGen {
	
	private static List<BiomeType> hydrogenBiomeList;
	private static boolean lakesEnabled;
	private static boolean regenhydrogen;
	private static int hydrogenLakeRarity;
	
	public static void mainRegistry(){
		initialiseWorldGen();
	}
	
	private final String name = "BluTech WorldGen";
	
	public String getFeatureName() {
		return name;
	}

	public static void initialiseWorldGen(){
		registerWorldGen(new WorldGenOre(), 1);
	}

	public static void registerWorldGen(IWorldGenerator worldGenClass, int weightedProberblity){
		GameRegistry.registerWorldGenerator(worldGenClass, weightedProberblity);
	}
}