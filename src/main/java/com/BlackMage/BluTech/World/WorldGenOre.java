package com.BlackMage.BluTech.World;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

import com.BlackMage.BluTech.Registry.BluTechBlocks;
import com.BlackMage.BluTech.Registry.BluTechGases;
import com.BlackMage.BluTech.Registry.BluTechLiquids;

public class WorldGenOre implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch (world.provider.getDimensionId()) {
		case -1:
			generateNether(random, chunkX * 16, chunkZ * 16, world);
			break;
		case 0:
			generateSurface(random, chunkX * 16, chunkZ * 16, world);
			break;
		case 1:
			generateEnd(random, chunkX * 16, chunkZ * 16, world);
			break;
		default:
			;
		}

	}

	private void addOre(Block block, Block blockSpawn, Random random, World world, int posX, int posZ, int minY, int maxY, int minVeinSize, int maxVeinSize, int spawnChance) {
		for (int i = 0; i < spawnChance; i++) {
			int defaultChunkSize = 16;

			int xPos = posX + random.nextInt(defaultChunkSize);
			int yPos = minY + random.nextInt(maxY - minY);
			int zPos = posZ + random.nextInt(defaultChunkSize);

			//new WorldGenMinable(block, (minVeinSize + random.nextInt(maxVeinSize - minVeinSize)), blockSpawn).generate(world, random, xPos, yPos, zPos);
		}
	}

	private void generateEnd(Random random, int chunkX, int chunkZ, World world) {
		//addOre(Block, Block.stone, random, world, chunkX, chunkZ, MinY, MaxY, MinVeinSize, MaxVeinSize, SpawnChance);
		//Everything before MinY is written. MinY and later needs a number
		//MinY defines lowest it will spawn, MaxY defines highest it will spawn
		//Min & Max Vein Size decide Vein Size
		//SpawnProbability decides how often it spawns.(No higher then 50 which is very common)

	}

	private void generateSurface(Random random, int chunkX, int chunkZ, World world) {
		//addOre(Block, Block.stone, random, world, chunkX, chunkZ, MinY, MaxY, MinVeinSize, MaxVeinSize, SpawnChance);
		//Everything before MinY is written. MinY and later needs a number
		//MinY defines lowest it will spawn, MaxY defines highest it will spawn
		//Min & Max Vein Size decide Vein Size
		//SpawnProbability decides how often it spawns.(No higher then 50 which is very common)
		addOre(BluTechBlocks.bluStoneOre, Blocks.stone, random, world, chunkX, chunkZ, 5, 16, 2, 5, 10);
		addOre(BluTechBlocks.lithiumOre, Blocks.stone, random, world, chunkX, chunkZ, 5, 20, 1, 4, 20);
		addOre(BluTechBlocks.carbonOre, Blocks.stone, random, world, chunkX, chunkZ, 1, 60, 2, 8, 20);
		addOre(BluTechGases.hydrogenGas, Blocks.stone, random, world, chunkX, chunkZ, 1, 40, 5, 20, 10);
		addOre(BluTechLiquids.liquidHydrogen, Blocks.stone, random, world, chunkX, chunkZ, 1, 78, 1, 10, 2);
	}

	private void generateNether(Random random, int chunkX, int chunkZ, World world) {
		//addOre(Block, Block.stone, random, world, chunkX, chunkZ, MinY, MaxY, MinVeinSize, MaxVeinSize, SpawnChance);
		//Everything before MinY is written. MinY and later needs a number
		//MinY defines lowest it will spawn, MaxY defines highest it will spawn
		//Min & Max Vein Size decide Vein Size
		//SpawnProbability decides how often it spawns.(No higher then 50 which is very common)

	}

}