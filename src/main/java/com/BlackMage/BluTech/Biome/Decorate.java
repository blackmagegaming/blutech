package com.BlackMage.BluTech.Biome;

import net.minecraft.util.BlockPos;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.gen.feature.WorldGenLiquids;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent.Decorate.EventType;
import net.minecraftforge.event.terraingen.TerrainGen;

import com.BlackMage.BluTech.Registry.BluTechLiquids;

public class Decorate extends BiomeDecorator
{
	public static boolean generateLakes;
	
	public Decorate()
    {
		this.generateLakes = true;
    }
	
	//protected void genDecorations(BiomeGenBase p_150513_1_)
	//below is a possible fix to the above
    protected void genDecorations(BiomeGenBase p_150513_1_, EventType LAKE_WATER)
    {
    	 MinecraftForge.EVENT_BUS.post(new DecorateBiomeEvent.Pre(currentWorld, randomGenerator,field_180294_c));
         this.generateOres();
         int i;
         int j;
         int k;
         int l;
         int i1;

    boolean doGen = TerrainGen.decorate(currentWorld, randomGenerator, field_180294_c, LAKE_WATER);
    if (doGen && this.generateLakes)
    {
        for (j = 0; j < 50; ++j)
        {
            k = this.field_180294_c.getX() + this.randomGenerator.nextInt(16) + 8;
            l = this.randomGenerator.nextInt(this.randomGenerator.nextInt(248) + 8);
            i1 = this.field_180294_c.getZ() + this.randomGenerator.nextInt(16) + 8;
            (new WorldGenLiquids(BluTechLiquids.liquidHydrogen)).generate(this.currentWorld, this.randomGenerator, new BlockPos(k, l, i1));
        }	
	
}
}
}