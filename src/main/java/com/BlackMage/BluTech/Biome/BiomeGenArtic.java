package com.BlackMage.BluTech.Biome;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenSnow;
import net.minecraft.world.gen.feature.WorldGenLakes;
import net.minecraft.world.gen.feature.WorldGenPumpkin;

import com.BlackMage.BluTech.Registry.BluTechLiquids;
import com.BlackMage.BluTech.api.Util.CellNoise;
import com.BlackMage.BluTech.api.Util.CliffCalculator;
import com.BlackMage.BluTech.api.Util.PerlinNoise;

public class BiomeGenArtic extends BiomeGenSnow {


	public BiomeGenArtic(int par1) {
		super(par1, true);
		this.spawnableCreatureList.clear();
        this.topBlock = Blocks.snow.getBlockState().getBaseState();
        this.fillerBlock = Blocks.packed_ice.getBlockState().getBaseState();
        this.spawnableCreatureList.clear();
        this.spawnableMonsterList.clear();
        this.spawnableWaterCreatureList.clear();
        this.setBiomeName("Artic");
		this.waterColorMultiplier = 0x99FFFF;
		this.setEnableSnow();
		this.setTemperatureRainfall(0.05F, 0.5F);
		this.theBiomeDecorator.treesPerChunk = -999;
		this.theBiomeDecorator.flowersPerChunk = -999;
		this.theBiomeDecorator.grassPerChunk = -999;
		this.theBiomeDecorator.sandPerChunk = -999;
		this.theBiomeDecorator.sandPerChunk2 = -999;
		Decorate.generateLakes = true;

	}
	
	public void rDecorate(World world, Random rand, int chunkX, int chunkY, PerlinNoise perlin, CellNoise cell, float strength)
    {      
		if(rand.nextInt((int)(15f / strength)) == 0)
		{
			int i2 = chunkX + rand.nextInt(16) + 8;
			int i8 = chunkY + rand.nextInt(16) + 8;
			int l4 = world.getHeight();
			if(l4 > 63 && l4 < 105)
			{
				(new WorldGenLakes(BluTechLiquids.liquidHydrogen)).generate(world, rand, new BlockPos(i2, l4, i8));
			}
		}
		
		if(rand.nextInt((int)(25f / strength)) == 0)
		{
			int j16 = chunkX + rand.nextInt(16) + 8;
			int j18 = rand.nextInt(128);
			int j21 = chunkY + rand.nextInt(16) + 8;
			(new WorldGenPumpkin()).generate(world, rand, new BlockPos(j16, j18, j21));
		}
		
    }

	public float rNoise(PerlinNoise perlin, CellNoise cell, int x, int y, float ocean, float border)
    {
		float h = (perlin.noise2(x / 100f, y / 100f) * 44f) + 59f;

		h += perlin.noise2(x / 28f, y / 28f) * 4;
		h += perlin.noise2(x / 18f, y / 18f) * 2;
		h += perlin.noise2(x / 8f, y / 8f) * 2;
		
		return h;
    }

	public void rReplace(Block[] blocks, byte[] metadata, int i, int j, int x, int y, int depth, Random rand, PerlinNoise perlin, CellNoise cell, float[] noise) 
	{
		float c = CliffCalculator.calc(x, y, noise);
		int cliff = 0;
		boolean gravel = false;
		
    	Block b;
		for(int k = 255; k > -1; k--)
		{
			b = blocks[(y * 16 + x) * 256 + k];
            if(b == Blocks.air)
            {
            	depth = -1;
            }
            else if(b == Blocks.stone)
            {
            	depth++;
            	
            	if(depth == 0)
            	{
            		if(k < 63)
            		{
            			gravel = true;
            		}
            		
        			if(c > 1.4f)
        			{
        				cliff = 1;
        			}
            		if(c > 1.9f)
        			{
        				cliff = 2;
        			}
            		
            		if(cliff == 1)
            		{
            			blocks[(y * 16 + x) * 256 + k] = rand.nextInt(3) == 0 ? Blocks.cobblestone : Blocks.stone; 
            		}
            		else if(cliff == 2)
            		{
        				blocks[(y * 16 + x) * 256 + k] = Blocks.stained_hardened_clay; 
        				metadata[(y * 16 + x) * 256 + k] = 9; 
            		}
            		else if(k < 63)
            		{
            			blocks[(y * 16 + x) * 256 + k] = Blocks.gravel;
            			gravel = true;
            		}
            		else
            		{
            			blocks[(y * 16 + x) * 256 + k] = Blocks.grass;
            		}
            	}
            	else if(depth < 6)
        		{
            		if(cliff == 1)
            		{
            			blocks[(y * 16 + x) * 256 + k] = Blocks.stone; 
            		}
            		else if(cliff == 2)
            		{
        				blocks[(y * 16 + x) * 256 + k] = Blocks.stained_hardened_clay; 
        				metadata[(y * 16 + x) * 256 + k] = 9; 
            		}
            		else if(gravel)
            		{
            			blocks[(y * 16 + x) * 256 + k] = Blocks.gravel;
            		}
            		else
            		{
            			blocks[(y * 16 + x) * 256 + k] = Blocks.dirt;
            		}
        		}
            }
		}
	}
	

}
