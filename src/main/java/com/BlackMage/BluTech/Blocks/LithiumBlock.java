package com.BlackMage.BluTech.Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import com.BlackMage.BluTech.CreativeTabsBluTech;

public class LithiumBlock extends Block implements BluTechBlock {
	
	private final String name = "LithiumBlock";
	
	public LithiumBlock()
	{
		super(Material.rock);
		setCreativeTab(CreativeTabsBluTech.BLOCKS.get());
		this.setUnlocalizedName(name);
	}
	
	public String getName()
	{
		return name;
	}

	public CreativeTabs getCreativeTab()
	{
		return CreativeTabsBluTech.BLOCKS.get();
	}
}
