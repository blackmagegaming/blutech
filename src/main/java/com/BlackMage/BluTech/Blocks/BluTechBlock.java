package com.BlackMage.BluTech.Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.Registry.RegisterHelper;

public interface BluTechBlock {
	
	public String getName();
	
	public CreativeTabs getCreativeTab();
}
