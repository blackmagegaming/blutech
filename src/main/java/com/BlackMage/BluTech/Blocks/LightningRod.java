package com.BlackMage.BluTech.Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import com.BlackMage.BluTech.CreativeTabsBluTech;

public class LightningRod extends Block implements BluTechBlock {
	
	private final String name = "LightningRod";
	
	public LightningRod()
	{
		super(Material.iron);
		setCreativeTab(CreativeTabsBluTech.BLOCKS.get());
		this.setUnlocalizedName(name);
	}
	
	public String getName()
	{
		return name;
	}
	
	public CreativeTabs getCreativeTab()
	{
		return CreativeTabsBluTech.BLOCKS.get();
	}
	//PowerAPI.CanProvidePower();

}
