package com.BlackMage.BluTech.Blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import com.BlackMage.BluTech.CreativeTabsBluTech;

public class CarbonBlock extends Block implements BluTechBlock {
	
	private final String name = "CarbonBlock";
	
	public CarbonBlock()
	{
		super(Material.rock);
		setCreativeTab(CreativeTabsBluTech.BLOCKS.get());
		this.setUnlocalizedName(name);
		  //@param level Harvest level:
	     //Wood:    0
	     //Stone:   1
	     //Iron:    2
	     //Diamond: 3
	     //Gold:    0
		setHarvestLevel("pickaxe",3);
		//sets how long it takes to harvest (3.0F is normal ore)
		setHardness(5.0F);
		//sets explosion resistance (5.0F is normal ore)
		setResistance(7.0F);
	}
	
	public Block getBlockDropped(int metadata, Random rand, int fortune)
    {
        return this;
    }
	
	public String getName()
	{
		return name;
	}

	public CreativeTabs getCreativeTab() {
		return CreativeTabsBluTech.BLOCKS.get();
	}

}
