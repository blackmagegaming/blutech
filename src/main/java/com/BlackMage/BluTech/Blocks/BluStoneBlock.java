package com.BlackMage.BluTech.Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneBlock extends Block implements BluTechBlock
{
	private final String name = "BluStoneBlock";
			
	public BluStoneBlock()
	{
		super(Material.rock);
		setCreativeTab(CreativeTabsBluTech.BLOCKS.get());
	}
	
	public String getName()
	{
		return name;
	}

	public CreativeTabs getCreativeTab() {
		return CreativeTabsBluTech.BLOCKS.get();
	}
	
}