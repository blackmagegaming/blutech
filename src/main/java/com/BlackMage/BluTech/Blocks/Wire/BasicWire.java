package com.BlackMage.BluTech.Blocks.Wire;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.BlackMage.BluTech.CreativeTabsBluTech;
import com.BlackMage.BluTech.api.PowerAPI;

public class BasicWire extends Block
{
	protected int Conductivity;
	protected  int Thickness;
	protected  double Resistance;
	protected  int Distance;
	protected  boolean IsInsulated;

	public static String name;
			
	public BasicWire()
	{
		super(Material.circuits);
	}
	
	public String getName()
	{
	return name;
	}
	
	public int getConductivity()
	{
		return Conductivity;
	}
	
	public int getThickness()
	{
		return Thickness;
	}
	
	public double getResistance()
	{
		return Resistance;
	}
	
	public int getDistance(){
		//check surrounding blocks for another wire and if found +1
		//if different wire (ex. copper instead of gold) return current distance and restart
		return Distance;
		
	}
	
	public boolean getIsInsulated()
	{
		return IsInsulated;
	}
	
	public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity){
		if (par5Entity instanceof EntityLivingBase) {
			if (IsInsulated == true) {
				
				//TODO Harm player
			
			}
		}
	}
	
	public double getPowerThru(){
		
		//Returns the power traveling thru the wire
		//int Conductivity,int Thickness,double Resistance,int Distance
		return PowerAPI.ElectricWire(this);
		
	}
}