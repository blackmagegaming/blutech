package com.BlackMage.BluTech.Blocks.Wire;

import net.minecraft.block.material.Material;

public class AluminumWire extends BasicWire {
	
	public AluminumWire()
	{
		Conductivity = 5;
		Thickness = 1;
		Resistance = 0.05;
		name = "AluminumWire";
		IsInsulated = false;
		this.setUnlocalizedName(name);
	}
	
}
