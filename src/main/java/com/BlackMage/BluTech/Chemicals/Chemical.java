package com.BlackMage.BluTech.Chemicals;

import net.minecraft.item.Item;

/**
 * Chemical - An item which represents some kind of aqueous chemical inside some kind of container. 
 *            It can have it's own properties that we specify, while still able to be registered in Minecraft.
 *            
 *            We can then use these to make shapeless reactions that can be done in our own GUI/Crafting system.
 *            
 * @author shadywatcher212
 *
 */
public abstract class Chemical extends Item {

	// TODO: Add properties shared by all chemicals
	
	// TODO: Add setters/getters to access/modify properties of chemicals
	
	// TODO: Anything else that defines how chemicals might behave
	
}
