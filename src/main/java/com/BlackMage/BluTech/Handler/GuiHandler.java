package com.BlackMage.BluTech.Handler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import com.BlackMage.BluTech.Containers.ContainerObsidianFurnace;
import com.BlackMage.BluTech.GUI.GuiObsidianFurnace;
import com.BlackMage.BluTech.TileEntity.TileEntityObsidianFurnace;

public class GuiHandler implements IGuiHandler {

    //returns an instance of the Container you made earlier
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world,
                    int x, int y, int z) {
            TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));
            if(tileEntity instanceof TileEntityObsidianFurnace){
                    return new ContainerObsidianFurnace(player.inventory, (TileEntityObsidianFurnace) tileEntity);
            }
            return null;
    }

    //returns an instance of the Gui you made earlier
    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world,
                    int x, int y, int z) {
            TileEntity tileEntity = world.getTileEntity(new BlockPos(x, y, z));
            if(tileEntity instanceof TileEntityObsidianFurnace){
                    return new GuiObsidianFurnace(player.inventory, (TileEntityObsidianFurnace) tileEntity);
            }
            return null;
	}

}
