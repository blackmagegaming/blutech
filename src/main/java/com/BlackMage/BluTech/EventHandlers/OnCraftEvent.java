package com.BlackMage.BluTech.EventHandlers;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import com.BlackMage.BluTech.Registry.BluTechAchievements;
import com.BlackMage.BluTech.Registry.BluTechItemTools;

public class OnCraftEvent {
	
	@SubscribeEvent
	public void CraftHammer(PlayerEvent.ItemCraftedEvent e){
		if(e.crafting.getItem().equals(BluTechItemTools.hammer)){
			e.player.addStat(BluTechAchievements.craftHammer, 1);
		}
	}

}
