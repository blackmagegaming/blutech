package com.BlackMage.BluTech.EventHandlers;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import com.BlackMage.BluTech.Registry.BluTechAchievements;
import com.BlackMage.BluTech.Registry.BluTechItemMaterials;

public class OnMineEvent {
	
	@SubscribeEvent
	public void MineBluStone(PlayerEvent.ItemPickupEvent e){
		if(e.pickedUp.getEntityItem().isItemEqual(new ItemStack(BluTechItemMaterials.bluStoneDust))){
			e.player.addStat(BluTechAchievements.aquireBluStone, 1);
		}
	}

}
