package com.BlackMage.BluTech.EventHandlers;

import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.entity.player.PlayerDestroyItemEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.HarvestCheck;
import net.minecraftforge.event.entity.player.PlayerEvent.LoadFromFile;
import net.minecraftforge.event.entity.player.PlayerEvent.SaveToFile;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerUseItemEvent;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.MultiPlaceEvent;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * This class hooks into Forge and manipulates the player's physical stats during run time.
 * It's an exhaustive list of possible player event listeners. Once registered on the EVENT_BUS,
 * it'll be fired when the events occur.
 * @author shadywatcher212
 *
 */

// TODO: Do actual manipulation to player stats
public class PhysicalStatEventHandler {

	// Static instance object to register on the event bus
	public static PhysicalStatEventHandler INSTANCE = new PhysicalStatEventHandler();
	
	@SubscribeEvent
	public void onEntityInteractEvent(EntityInteractEvent event) {
		
	}
	
	@SubscribeEvent
	public void onEntityItemPickupEvent(EntityItemPickupEvent event) {
		
	}
	
	@SubscribeEvent
	public void onFillBucketEvent(FillBucketEvent event) {
		
	}
	
	@SubscribeEvent
	public void onPlayerDestroyItemEvent(PlayerDestroyItemEvent event) {
		
	}
	
	@SubscribeEvent
	public void onPlayerDropsEvent(PlayerDropsEvent event) {
		
	}
	
	@SubscribeEvent
	public void onPlayerInteractEvent(PlayerInteractEvent event) {
		
	}
	
	// NOTE: There can be different kinds of these events, (Start, Stop, Finish, Tick)
	@SubscribeEvent
	public void onPlayerUseItemEvent(PlayerUseItemEvent event) {
		
	}
	
	/**
	 * Block Events
	 */
	@SubscribeEvent
	public void onBlockBreakEvent(BreakEvent event) {
		
	}
	
	@SubscribeEvent
	public void onBlockPlacedEvent(PlaceEvent event) {
		
	}
	
	// Called when a block placement results in multiple blocks being placed (ie placing a bed)
	@SubscribeEvent
	public void onBlockMultiPlaceEvent(MultiPlaceEvent event) {
		
	}
	
	// Event fired when a player attempts to harvest a block
	@SubscribeEvent
	public void onBlockHarvestCheck(HarvestCheck event) {
		
	}
	
	/**
	 * Misc events that we may need for saving/loading player science stats.
	 */
	@SubscribeEvent
	public void onPlayerLoadFromFile(LoadFromFile event) {
		
	}
	
	@SubscribeEvent
	public void onPlayerSaveToFile(SaveToFile event) {
		
	}
}
