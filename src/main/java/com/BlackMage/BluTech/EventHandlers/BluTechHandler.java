package com.BlackMage.BluTech.EventHandlers;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class BluTechHandler {
	public static BluTechHandler INSTANCE = new BluTechHandler();
	public Map<Block, Item> buckets = new HashMap<Block, Item>();

	@SubscribeEvent
	public void onBucketFill(FillBucketEvent event){

		ItemStack result = fillBucket(event.world, event.target);

		if (result == null){
			return;
		}

		event.result = result;
		event.setResult(Result.ALLOW);

	}
	/*private ItemStack fillBucket(World world, MovingObjectPosition pos){

		Block block = world.getBlockState(pos.getBlockPos()).getBlock();

		Item bucket = buckets.get(block);

		if (bucket != null && world.getBlockState(pos.getBlockPos()) == null){
			world.setBlockToAir(pos.getBlockPos());
			return new ItemStack(bucket);
		}

		return null;


	}*/

	private ItemStack fillBucket(World world, MovingObjectPosition target) {
		// TODO Auto-generated method stub
		return null;
	}

}
