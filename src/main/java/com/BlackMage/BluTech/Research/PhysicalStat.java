package com.BlackMage.BluTech.Research;

import java.io.Serializable;

import net.minecraft.block.Block;


/*
 * PhysicalStat - A container to keep track of statistics of an individual block for a player.
 */
public class PhysicalStat implements Serializable {

	// Block being studied - can't be serialized
	protected transient Block block;
	
	// Block reference
	protected String blockName;
	
	// Number of times walked over - Sound Factor
	protected int steps;
	
	// Number of times mined - Hardness Factor
	protected int mined;
	
	// Number of times looked at(?) - Look Factor
	protected int lookedAt;
	 
	// Number of times interacted with - Uses Factor
	protected int interactedWith;
	
	/* Constructors */
	
	public PhysicalStat(Block b) {
		this.block          = b;
		this.blockName      = b.getUnlocalizedName();
		this.steps          = 0;
		this.mined          = 0;
		this.lookedAt       = 0;
		this.interactedWith = 0;
	}
	
	/* Getters and Setters */
	
	/*
	 * Function    - getBlock()
	 * Description - Returns the block of which this stat belongs to
	 */
	public Block getBlock() {
		return this.block;
	}
	
	/*
	 * Function    - getSteps()
	 * Description - Returns the number of steps a player has stepped on the specified block
	 */
	public int getSteps() {
		return this.steps;
	}
	
	/*
	 * Function    - getLookedAt()
	 * Description - Returns the number of times a player has looked at a block
	 */
	public int getLookedAt() {
		return this.lookedAt;
	}
	
	/*
	 * Function    - getInteractedWith()
	 * Description - Returns the number of times a player has interacted with a block.
	 */
	public int getInteractedWith() {
		return this.interactedWith;
	}
	
	/*
	 * Function    - getMined()
	 * Description - Returns the number of times a block was mined
	 */
	public int getMined() {
		return mined;
	}
	
	/*
	 * Function    - setBlock()
	 * Description - Sets the block of which the statistic is being tracked.
	 */
	protected void setBlock(Block b) {
		this.block = b;
	}
	
	/* Core Functions */
	
	/*
	 * Function    - increaseSteps()
	 * Description - Increase the number of steps walked by the player by 1.
	 */
	public void increaseSteps() {
		// Make sure that the number of steps doesn't exceed the max possible integer value
		if(steps != Integer.MAX_VALUE) 
			steps++;
	}
	
	/*
	 * Function    - increaseMined()
	 * Description - Increase the number of times a player has mined a specific block, making sure
	 *               the amount doesn't exceed the maximum possible integer value 
	 */
	public void increaseMined() {
		// Make sure that the number of times mined doesn't exceed the max possble integer value
		if(mined != Integer.MAX_VALUE) {
			mined++;
		}
	}
	
	/*
	 * Function    - increaseLookedAt()
	 * Description - Increase the number of times a player has looked at a specific block by one,
	 *               making sure that the amount doesn't exceed the max possible integer value.
	 */
	public void increaseLookedAt() {
		// Make sure that the number of times lookedAt doesn't exceed the max possible integer value
		if(lookedAt != Integer.MAX_VALUE) {
			lookedAt++;
		}
	}
	
	/*
	 * Function    - increaseInteractWith()
	 * Description - Increase the number of times a player has interacted with a specific block by one,
	 *               making sure that the amount doesn't exceed the max possible integer value.
	 */
	public void increaseInteractWith() {
		// Make sure the number of times a player interacts with a block doesn't exceed the max possible integer value'
		if(interactedWith != Integer.MAX_VALUE) {
			interactedWith++;
		}
	}
}
