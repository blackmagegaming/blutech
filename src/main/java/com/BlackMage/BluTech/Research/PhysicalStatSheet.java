package com.BlackMage.BluTech.Research;

import java.io.Serializable;
import java.util.LinkedList;

import net.minecraft.block.Block;
import net.minecraftforge.fml.common.registry.GameData;

/*
 * PhysicalStatSheet - A class that wraps around a Linked List of PhysicalStat objects.
 * 
 *                     This class should probably be serialized.
 */
public class PhysicalStatSheet implements Serializable {
	
	// Holds physical stats
	protected LinkedList<PhysicalStat> stats;
	
	public PhysicalStatSheet() {
		
	}
	
	/*
	 * Function    - copyBlocks()
	 * Description - Copies the block references from the game registry and creates Physical stats for each specific block.
	 */
	protected void copyBlocks() {
		// This code with go through the entire block registry in the game
		for(Object o : GameData.getBlockRegistry().getKeys()) {
			String s = (String) o;
			Block b = GameData.getBlockRegistry().getObject(s);
			
			// Add the stat to the list
			stats.add(new PhysicalStat(b));
		}
	}
	
	/*
	 * Function    - getStat()
	 * Description - Returns the PhysicalStat object of the specified Block.
	 *               This could fail if we aren't loading the block references back into the 
	 *               PhysicalStat object correctly.
	 *               
	 *               Returns null on failure.
	 */
	public PhysicalStat getStat(Block b) {
		for(PhysicalStat s : stats) {
			if(s.block.equals(b)) {
				return s;
			}
		}
		
		return null;
	}
}
