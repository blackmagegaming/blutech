package com.BlackMage.BluTech.Research;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.registry.GameData;

public class PhysicalStats {
	
	// Stats of players by name - we'll need to be careful of player display names
	private static final HashMap<String, PhysicalStatSheet> players = new HashMap<String, PhysicalStatSheet>();
	
	/*
	 * Function    - LoadStats()
	 * Description - Loads the Stat sheet for an individual player using Java Object Serialization
	 */
	public static void LoadStats(EntityPlayer p) {
		File f; 
		PhysicalStatSheet s;
		
		// Player isn't in memory
		if(players.get(p.getCommandSenderEntity()) == null) {
			
			// See if the file exists
			f = new File(p.getCommandSenderEntity() + ".stats");
			
			if(f.exists()) {
				// Load file
				try {
					ObjectInputStream ois = new ObjectInputStream(new FileInputStream(p.getCommandSenderEntity() + ".stats"));
					s = (PhysicalStatSheet) ois.readObject();
					ois.close();
					
					// Blocks can't be written - therefore we need to find and set the block reference
					for(PhysicalStat stat : s.stats) {
						stat.setBlock(GameData.getBlockRegistry().getObject(stat.blockName));
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

			} else {			
				// Create a new blank sheet
				s = new PhysicalStatSheet();
				s.copyBlocks();
				players.put(p.getCommandSenderEntity().getName(), s);
			}
		}
	}
	
	/*
	 * Function    - SaveStats()
	 * Description - Writes out an object file using Java Object serialization
	 */
	public static void SaveStats(EntityPlayer p) {
		// Write out the file for a single player
		PhysicalStatSheet st = players.get(p.getCommandSenderEntity());
		
		if(st != null) {
			// Write the file
			try {
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(p.getCommandSenderEntity() + ".stats"));
				oos.writeObject(st);
				oos.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/*
	 * Function    - GetPlayerStats()
	 * Description - Returns the specified player's stat-sheet.
	 *               
	 *               Returns null on failure.
	 */
	public static PhysicalStatSheet GetPlayerStats(EntityPlayer p) {
		if(p == null || players.get(p.getCommandSenderEntity()) == null) {
			return null;
		} else return players.get(p.getCommandSenderEntity());
	}
}
