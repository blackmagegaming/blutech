package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneIngot extends Item {
	
	private final String name = "BluStoneIngot";
	
	public BluStoneIngot()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneIngot");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}

}
