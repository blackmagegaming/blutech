package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class ReinforcedBluStoneGem extends Item 
{
	
	private final String name = "ReinforcedBluStoneGem";
	
public ReinforcedBluStoneGem() {
		setUnlocalizedName(BluTech.MODID + "_" + "ReinforcedBluStoneGem");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}

public String getName()
{
return name;
}

}
