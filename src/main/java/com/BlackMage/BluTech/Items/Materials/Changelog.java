package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemEditableBook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.world.World;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class Changelog extends ItemEditableBook
{
	
	private final String name = "Changelog";
	
   public Changelog()
   {
    super();
    this.setMaxStackSize(1);
    setUnlocalizedName(BluTech.MODID + "_" + "Changelog");
    setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
   }

   private NBTTagList putTableOfContents(NBTTagList bookTagList)
   {
    NBTTagCompound tag = new NBTTagCompound();
    NBTTagList bookPages = new NBTTagList();
	   bookPages.appendTag(new NBTTagString("Page 1"));
	 
	  return bookTagList;
   }
  
   public void onUpdate(ItemStack itemStack, World world, Entity entity, int unknownInt, boolean unknownBool)
   {
	  NBTTagList bookTagList = new NBTTagList();
	  NBTTagCompound tag = new NBTTagCompound();
	  bookTagList = putTableOfContents(bookTagList);
	 
	  itemStack.setTagInfo("pages", bookTagList);
	  itemStack.setTagInfo("author", new NBTTagString("BlackMage"));
	  itemStack.setTagInfo("title", new NBTTagString("BluTech Changelog"));
   }
   public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
   {
	   par3EntityPlayer.displayGUIBook(par1ItemStack);
	   return par1ItemStack;
   }
   
   public String getName()
	{
	return name;
	}
  
}