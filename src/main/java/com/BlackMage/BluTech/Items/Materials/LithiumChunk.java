package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class LithiumChunk extends Item {
	
	private final String name = "LithiumChunk";

	public LithiumChunk()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "LithiumChunk");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
