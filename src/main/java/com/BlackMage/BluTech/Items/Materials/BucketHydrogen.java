package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.block.Block;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBucket;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BucketHydrogen extends ItemBucket{
	
	private final String name = "HydrogenBucket";

	public BucketHydrogen(Block par1) {
		super(par1);
		this.setMaxStackSize(16);
		this.setContainerItem(Items.bucket);
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
		setUnlocalizedName(BluTech.MODID + "_BucketHydrogen");
		setContainerItem(Items.bucket);
		
	}
	
	public String getName()
	{
	return name;
	}

}