package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;


public class BluStoneCrystal extends Item 
{
	
	private final String name = "BluStoneCrystal";
	
	public BluStoneCrystal()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneCrystal");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}