package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneShard extends Item 
{
	
	private final String name = "BluStoneShard";
	
	public BluStoneShard()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneShard");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}