package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneGem extends Item 
{
	
	private final String name = "BluStoneGem";
	
	public BluStoneGem()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneGem");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}