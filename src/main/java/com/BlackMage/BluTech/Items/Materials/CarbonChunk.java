package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class CarbonChunk extends Item {
	
	private final String name = "CarbonChunk";

	public CarbonChunk()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "CarbonChunk");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
