package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class InfusedStick extends Item 
{
	
	private final String name = "InfusedStick";
	
	public InfusedStick()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "InfusedStick");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}