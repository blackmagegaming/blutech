package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneChunk extends Item 
{
	
	private final String name = "BluStoneChunk";
	
public BluStoneChunk() {
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneChunk");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
		}

public String getName()
{
return name;
}

}
