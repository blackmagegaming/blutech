package com.BlackMage.BluTech.Items.Materials;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneDust extends Item {
	
	private final String name = "BluStoneDust";

	public BluStoneDust()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneDust");
		setCreativeTab(CreativeTabsBluTech.MATERIALS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
