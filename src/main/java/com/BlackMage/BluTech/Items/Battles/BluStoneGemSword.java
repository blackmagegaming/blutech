package com.BlackMage.BluTech.Items.Battles;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneGemSword extends Item{
	
	private final String name = "BluStoneGemSword";

	public BluStoneGemSword(){
		super();
		setUnlocalizedName(BluTech.MODID + "_" + name);
		setCreativeTab(CreativeTabsBluTech.BATTLE.get());
		}
	
	public String getName()
	{
	return name;
	}

}
