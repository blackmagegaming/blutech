package com.BlackMage.BluTech.Items.Battles;

import net.minecraft.item.ItemArmor;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;


public class BluStoneArmor extends ItemArmor
{
	
	private final String name = "BluStoneArmor";

	public BluStoneArmor(int par1,
			ArmorMaterial par2EnumArmorMaterial, int par3, int par4) {
		super(par2EnumArmorMaterial, par3, par4);
		setUnlocalizedName(BluTech.MODID + "_" + name);
		setCreativeTab(CreativeTabsBluTech.BATTLE.get());
	}


/*
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
	if(itemID == BlueStoneMod.bluestoneHelmet.itemID || itemID == BlueStoneMod.bluestoneChestplate.itemID || itemID == BlueStoneMod.bluestoneBoots.itemID)
	{
	return "bluestone:textures/items/bluestone_1.png";
	}
	if(itemID == BlueStoneMod.bluestoneLeggings.itemID)
	{
	return "bluestone:textures/items/bluestone_2.png";
	}
	else return null;
	
	}
	
	@Override
	public void registerIcons(IconRegister iconRegister) {
		
		if(itemID == BlueStoneMod.bluestoneHelmet.itemID){
	
	                 itemIcon = iconRegister.registerIcon("bluestone:ItemBluestoneHelmet");
		}
		if(itemID == BlueStoneMod.bluestoneChestplate.itemID){
			
            itemIcon = iconRegister.registerIcon("bluestone:ItemBluestoneChestplate");
}
		if(itemID == BlueStoneMod.bluestoneLeggings.itemID){
			
            itemIcon = iconRegister.registerIcon("bluestone:ItemBluestoneLeggings");
}
		if(itemID == BlueStoneMod.bluestoneBoots.itemID){
			
            itemIcon = iconRegister.registerIcon("bluestone:ItemBluestoneBoots");
}
		
}*/
	
	public String getName()
	{
	return name;
	}
	
}