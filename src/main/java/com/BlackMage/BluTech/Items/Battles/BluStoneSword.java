
package com.BlackMage.BluTech.Items.Battles;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneSword extends Item {
	
	private final String name = "BluStoneSword";
	
public BluStoneSword(){
setUnlocalizedName(BluTech.MODID + "_" + "BluStoneSword");
setCreativeTab(CreativeTabsBluTech.BATTLE.get());
}

public String getName()
{
return name;
}

}