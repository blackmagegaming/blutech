package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneGemShovel extends Item {
	
	private final String name = "BluStoneGemShovel";

	public BluStoneGemShovel()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneGemShovel");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
