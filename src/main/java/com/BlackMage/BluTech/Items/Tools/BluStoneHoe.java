package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneHoe extends Item {
	
	private final String name = "BluStoneHoe"; 

	public BluStoneHoe()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneHoe");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
