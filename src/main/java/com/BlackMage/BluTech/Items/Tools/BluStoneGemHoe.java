package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneGemHoe extends Item {
	
	private final String name = "BluStoneGemHoe";

	public BluStoneGemHoe()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneGemHoe");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
