package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneGemPickaxe extends Item/*Pickaxe*/ {
	
	private final String name = "BluStoneGemPickaxe";

	public BluStoneGemPickaxe(/*ToolMaterial material, String name*/)
	{
		//super(material);
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneGemPickaxe");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
