package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class Hammer extends Item {
	
	private final String name = "Hammer";

	public Hammer()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "Hammer");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
