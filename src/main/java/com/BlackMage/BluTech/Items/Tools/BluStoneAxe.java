package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneAxe extends Item {
	
	private final String name = "BluStoneAxe";

	public BluStoneAxe()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneAxe");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
