package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneShovel extends Item {
	
	private final String name = "BluStoneShovel";

	public BluStoneShovel()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneShovel");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
