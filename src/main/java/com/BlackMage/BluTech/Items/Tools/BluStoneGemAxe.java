package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStoneGemAxe extends Item {
	
	private final String name = "BluStoneGemAxe";

	public BluStoneGemAxe()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStoneGemAxe");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
