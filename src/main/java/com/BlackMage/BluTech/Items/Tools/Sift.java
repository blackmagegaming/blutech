package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class Sift extends Item {
	
	private final String name = "Sift";

	public Sift()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "Sift");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
