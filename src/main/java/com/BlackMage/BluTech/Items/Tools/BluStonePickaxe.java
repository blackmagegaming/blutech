package com.BlackMage.BluTech.Items.Tools;

import net.minecraft.item.Item;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

public class BluStonePickaxe extends Item {
	
	private final String name = "BluStonePickaxe";

	public BluStonePickaxe()
	{
		setUnlocalizedName(BluTech.MODID + "_" + "BluStonePickaxe");
		setCreativeTab(CreativeTabsBluTech.TOOLS.get());
	}
	
	public String getName()
	{
	return name;
	}
	
}
