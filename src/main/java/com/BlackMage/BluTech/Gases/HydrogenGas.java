package com.BlackMage.BluTech.Gases;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

	
public class HydrogenGas extends BasicGas {
	
	private final String name = "HydrogenGas";

	{Pressure = 3; canExplode = true; canBurn = true; isDestructive = false; Weight = 10;}
	
	public HydrogenGas()
	{
		super();
		setCreativeTab(CreativeTabsBluTech.Gases.get());
		setUnlocalizedName(name);
	}
	
	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getName()
	{
	return name;
	}

}
