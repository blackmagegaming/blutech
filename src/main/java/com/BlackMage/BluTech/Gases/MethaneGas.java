package com.BlackMage.BluTech.Gases;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.CreativeTabsBluTech;

	
public class MethaneGas extends BasicGas {
	
	private final String name = "MethaneGas";

	{Pressure = 3; canExplode = true; canBurn = true; isDestructive = false; Weight = 15;}
	
	public MethaneGas()
	{
		super();
		setCreativeTab(CreativeTabsBluTech.Gases.get());
		setUnlocalizedName(name);
	}
	
	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getName()
	{
	return name;
	}

}
