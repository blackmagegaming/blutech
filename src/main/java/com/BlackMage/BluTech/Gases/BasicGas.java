package com.BlackMage.BluTech.Gases;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;

public abstract class BasicGas extends BlockContainer {
	
	public static int Pressure;
	public boolean canExplode;
	public boolean canBurn;
	public boolean isDestructive;
	public Fluid blocksFluid;
	public static int Weight;//oxygen has the weight of 5, heavier sinks lighter floats

	
	public BasicGas()
	{
		super(Material.circuits);
		setLightOpacity(lightOpacity);
		this.setTickRandomly(true);
		this.disableStats();
		this.setBlockUnbreakable();
	}
	
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
	{
		return null;
	}
	
	public void gasmovement(World world, int x, int y, int z, Block block){
		
		Block[] blocks = { 
				// Basic 3 directions
				world.getBlockState(new BlockPos(x + 1, y, z)).getBlock(),
				world.getBlockState(new BlockPos(x - 1, y, z)).getBlock(),
				world.getBlockState(new BlockPos(x, y + 1, z)).getBlock(),
				world.getBlockState(new BlockPos(x, y - 1, z)).getBlock(),
				world.getBlockState(new BlockPos(x, y, z + 1)).getBlock(),
				world.getBlockState(new BlockPos(x, y, z - 1)).getBlock()
		};
		for(Block b : blocks) {
			if (Weight < 5){
				
			}
			else if(Weight > 5){
				
			}
			else{
			}
		}
	}
	
	
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		// The Block passed is actually the block that was REMOVED, so we need the new one that is there
		Block[] blocks = { 
				// Basic 3 directions
				world.getBlockState(new BlockPos(x + 1, y, z)).getBlock(),
				world.getBlockState(new BlockPos(x - 1, y, z)).getBlock(),
				world.getBlockState(new BlockPos(x, y + 1, z)).getBlock(),
				world.getBlockState(new BlockPos(x, y - 1, z)).getBlock(),
				world.getBlockState(new BlockPos(x, y, z + 1)).getBlock(),
				world.getBlockState(new BlockPos(x, y, z - 1)).getBlock()
		};
		
		for(Block b : blocks) {
			if(b.equals(Blocks.fire) || (b.equals(Blocks.lava) || b.equals(Blocks.flowing_lava) || b.equals(Blocks.torch)))
			{
					FlameContact(world, x, y, z);
			}
		}
	}
	
	public void explode(World world, int x, int y, int z)
	{
		// Make sure that the gas is able to explode
		if(canExplode && world.isRemote == false)
		{
			// Delete the current block and cause the explosion!
			world.setBlockToAir(new BlockPos(x, y, z));
			world.createExplosion(null, (double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, 4.0F, true);

		}
	}
	
	public void FlameContact(World world, int x, int y, int z)
	{
		if(canExplode)
		{
			this.explode(world, x, y, z);
		}
		else if(canBurn)
		{
			int tempY = y - 1;
			while(world.getBlockState(new BlockPos(x, tempY, z)).getBlock() == this || world.getBlockState(new BlockPos(x, tempY, z)).getBlock() == Blocks.air || world.getBlockState(new BlockPos(x, tempY, z)).getBlock().isReplaceable(world, new BlockPos(x, tempY, z)))
			{
				tempY--;
			}
			
			tempY++;
			
			world.setBlockToAir(new BlockPos(x, y, z));
			world.removeTileEntity(new BlockPos(x, y, z));
			
			// FIXME: How do we change blocks to fire?
			world.setBlockState(new BlockPos(x, y, z), Blocks.fire.getDefaultState());
		}
	}
	
	public boolean setBlockTransparent()
	{
		return true;
	}

	public boolean isReplaceable(IBlockAccess world, int i, int j, int k)
	{
		return true;
	}
	

	public void onBlockExploded(World world, int x, int y, int z, Explosion explosion)
	{	
		FlameContact(world, x, y, z);
	}
	
	/**
     * Returns the quantity of items to drop on block destruction.
     */
	@Override
    public int quantityDropped(Random par1Random)
    {
        return 0;
    }

    /**
     * Returns which pass should this block be rendered on. 0 for solids and 1 for alpha
   	@Override
    public int getRenderBlockPass()
    {
        return 1;
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
	@Override
    public boolean isOpaqueCube()
    {
        return false;
    }
	
	public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity) {
	if (par5Entity instanceof EntityLivingBase) {
		((EntityLivingBase) par5Entity).addPotionEffect(new PotionEffect(Potion.confusion.getId(), 100, 2));
		}
	 }


    /**
     * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
     */
//	@Override
//    public boolean renderAsNormalBlock()
//    {
//        return false;
//    }

	
}
