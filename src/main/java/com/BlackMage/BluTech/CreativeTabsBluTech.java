package com.BlackMage.BluTech;

import java.util.Locale;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.BlackMage.BluTech.Registry.BluTechBlocks;
import com.BlackMage.BluTech.Registry.BluTechGases;
import com.BlackMage.BluTech.Registry.BluTechItemBattles;
import com.BlackMage.BluTech.Registry.BluTechItemMaterials;
import com.BlackMage.BluTech.Registry.BluTechItemTools;


public enum CreativeTabsBluTech {

	BLOCKS,
	TOOLS,
	MATERIALS,
	BATTLE,
	Gases;
	private final CreativeTabs tab;

	private CreativeTabsBluTech() {
		tab = new Tab();
	}

	public CreativeTabs get() {
		return tab;
	}

	private String getLabel() {
		return "BluTech" + name().toLowerCase(Locale.ENGLISH);
	}

	private ItemStack getItem() {
		switch (this) {
		case BLOCKS:
			return new ItemStack (BluTechBlocks.bluStoneOre, 1);
		case TOOLS:
			return new ItemStack (BluTechItemTools.hammer, 1);
		case MATERIALS:
			return new ItemStack (BluTechItemMaterials.bluStoneDust, 1);
		case BATTLE:
			return new ItemStack (BluTechItemBattles.bluStoneGemSword, 1);
		case Gases:
			return new ItemStack (BluTechGases.hydrogenGas, 1);
		}
		
		return new ItemStack (BluTechItemMaterials.carbonChunk, 0);
	}

	private final class Tab extends CreativeTabs {

		private Tab() {
			super(getLabel());
		}

		@Override
		public ItemStack getIconItemStack() {
			return getItem();
		}

		@Override
		public Item getTabIconItem() {
			return getItem().getItem();
		}
	}
}