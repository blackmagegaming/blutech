package com.BlackMage.BluTech;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.DungeonHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

import com.BlackMage.BluTech.Blocks.BluStoneBlock;
import com.BlackMage.BluTech.EventHandlers.BluTechHandler;
import com.BlackMage.BluTech.EventHandlers.OnCraftEvent;
import com.BlackMage.BluTech.EventHandlers.OnMineEvent;
import com.BlackMage.BluTech.EventHandlers.PhysicalStatEventHandler;
import com.BlackMage.BluTech.Handler.GuiHandler;
import com.BlackMage.BluTech.Items.Battles.BluStoneGemSword;
import com.BlackMage.BluTech.Proxy.CommonProxy;
import com.BlackMage.BluTech.Registry.BluTechBlocks;
import com.BlackMage.BluTech.Registry.BluTechItemBattles;
import com.BlackMage.BluTech.Registry.BluTechItemMaterials;
import com.BlackMage.BluTech.Registry.BluTechRegistry;
import com.BlackMage.BluTech.World.WorldGen;

@Mod(modid = BluTech.MODID, version = "Alpha-" + BluTech.VERSION)
public class BluTech
{

    public static final String MODID = "blutech";
    public static final String VERSION = "0.0.0.7";

    @SidedProxy(clientSide = "com.BlackMage.BluTech.Proxy.ClientProxy", serverSide = "com.BlackMage.BluTech.Proxy.CommonProxy")
    public static CommonProxy proxy;
    
    public static boolean gasBlocksCanExplode = true;
    public static boolean gasBlocksInWorld = true;
    
    static ArmorMaterial blustoneGemArmor = EnumHelper.addArmorMaterial("BluStone", null, 3000, new int[] {7,12,10,6}, 30);
    static ToolMaterial EnumToolMaterialBluStone= EnumHelper.addToolMaterial("BluStone", 2, 300, 7.0F, 3, 15);
    static ToolMaterial EnumToolMaterialBluStoneGem= EnumHelper.addToolMaterial("BluStoneGem", 3, 3000, 15.0F, 5, 25);
    
    @EventHandler
	public static void PreLoad(FMLPreInitializationEvent PreEvent){

		WorldGen.mainRegistry();
		
	//BluStoneGemPickaxe = new BluStonePickaxe( EnumToolMaterialBluStone, "BluStoneGemPickaxe");
	//GameRegistry.registerItem(BluStonePickaxe, "BluStonePickaxe");
		 

	}
    
  @EventHandler
    public void load(FMLInitializationEvent event) {
	  
	  	MinecraftForge.EVENT_BUS.register(BluTechHandler.INSTANCE);
	  	
	  	// Register Physical Stat tracking
	  	MinecraftForge.EVENT_BUS.register(PhysicalStatEventHandler.INSTANCE);
	  	
	  	//EventHandlers
    	FMLCommonHandler.instance().bus().register(new OnMineEvent());
    	FMLCommonHandler.instance().bus().register(new OnCraftEvent());
    	NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		// Console messages
    	System.out.println("BluTech Mod By _BlackMage_ & the ForgeWare Team");
		System.out.println("A ForgeWare Mod");
		System.out.println("This mod is in BETA and may cause crashes!");
		
		// Register everything BluTech
		BluTechRegistry.register();
		
		
		//smelting
		GameRegistry.addSmelting(BluTechItemMaterials.bluStoneChunk, new ItemStack(BluTechItemMaterials.bluStoneIngot), 0.1F);

		ChestGenHooks.addItem(ChestGenHooks.MINESHAFT_CORRIDOR, new WeightedRandomChestContent(new ItemStack(BluTechItemMaterials.bluStoneDust), 1, 3, 3));
		
		DungeonHooks.addDungeonMob("Creeper", 100);
		
		/*
		 * FIXME
		 * Shadey only added these two for the render registry was not sure how you wanted to do the render
		 */
		
		//register renders
    	if(event.getSide() == Side.CLIENT)
    	{
    	RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
    
    	//blocks
    	renderItem.getItemModelMesher().register(Item.getItemFromBlock(BluTechBlocks.bluStoneBlock), 0, new ModelResourceLocation(MODID + ":" + ((BluStoneBlock) BluTechBlocks.bluStoneBlock).getName(), "inventory"));
    
    	//items
    	renderItem.getItemModelMesher().register(BluTechItemBattles.bluStoneGemSword, 0, new ModelResourceLocation(MODID + ":" + ((BluStoneGemSword) BluTechItemBattles.bluStoneGemSword).getName(), "inventory"));
    	}

    }
    
    @EventHandler
    public void postinit(FMLPostInitializationEvent event){

    }
}