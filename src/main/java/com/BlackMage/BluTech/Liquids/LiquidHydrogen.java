package com.BlackMage.BluTech.Liquids;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialLiquid;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.BlackMage.BluTech.BluTech;
import com.BlackMage.BluTech.Registry.BluTechFluids;

public class LiquidHydrogen extends BlockFluidClassic{
	
	private final String name = "LiquidHydrogen";

	/*
	@SideOnly(Side.CLIENT)
    protected IIcon stillIcon;
    @SideOnly(Side.CLIENT)
    protected IIcon flowingIcon;
    */
	public LiquidHydrogen(Fluid fluid, Material material) {
		super(fluid, material);
		this.setUnlocalizedName(name);
	}
	
	public LiquidHydrogen() {
		super(BluTechFluids.fluidHydrogen, new MaterialLiquid(MapColor.silverColor));
	}
	/*
	@Override
    public IIcon getIcon(int side, int meta) {
            return (side == 0 || side == 1)? stillIcon : flowingIcon;
    }
   
    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister register) {
            stillIcon = register.registerIcon(BluTech.MODID + ":" + "hydrogen_still");
            flowingIcon = register.registerIcon(BluTech.MODID + ":" + "hydrogen_flow");
    }
    */
	@Override
    public boolean canDisplace(IBlockAccess world, BlockPos pos) {
            if (world.getBlockState(pos).getBlock().getMaterial().isLiquid()) return false;
            return super.canDisplace(world, pos);
    }
   
    @Override
    public boolean displaceIfPossible(World world, BlockPos pos) {
            if (world.getBlockState(pos).getBlock().getMaterial().isLiquid()) return false;
            return super.displaceIfPossible(world, pos);
    }
    
    public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity) {
      	 if (par5Entity instanceof EntityLivingBase) {
      	  ((EntityLivingBase) par5Entity).addPotionEffect(new PotionEffect(Potion.harm.getId(), 60, 0));
      	  ((EntityLivingBase) par5Entity).addPotionEffect(new PotionEffect(Potion.confusion.getId(), 100, 2));
      	 }
    }  	
    
    @Override
    protected void flowIntoBlock(World world, BlockPos pos, int meta)
    {
        if (meta < 0) return;
        if (world.getBlockState(pos).getBlock().getMaterial() == Material.water){
        	world.setBlockState(pos, Blocks.ice.getBlockState().getBaseState(), 3);
        	//world.setBlock(pos, Blocks.ice, meta, 3);
        	return;
        }
        if (displaceIfPossible(world, pos))
        {
            world.setBlockState(pos, this.getBlockState().getBaseState(), 3);
        }
    }
    
    
    //breaks drain
    public void onNeighborBlockChange(World world, int p_149695_2_, int p_149695_3_, int p_149695_4_, Block p_149695_5_)
    {
        this.makeIce(world, p_149695_2_, p_149695_3_, p_149695_4_);
    }
    
    private void makeIce(World world, int x, int y, int z)
    {
    	BlockPos pos = new BlockPos(x, y, z);
        if (world.getBlockState(pos).getBlock() == this || world.getBlockState(pos).getBlock().getMaterial()== Material.water)
        {
                boolean flag = false;

                if (flag || world.getBlockState(new BlockPos(x, y, z - 1)).getBlock().getMaterial() == Material.water)
                {
                    flag = true;
                }

                if (flag || world.getBlockState(new BlockPos(x, y, z + 1)).getBlock().getMaterial() == Material.water)
                {
                    flag = true;
                }

                if (flag || world.getBlockState(new BlockPos(x - 1, y, z)).getBlock().getMaterial() == Material.water)
                {
                    flag = true;
                }

                if (flag || world.getBlockState(new BlockPos(x + 1, y, z)).getBlock().getMaterial() == Material.water)
                {
                    flag = true;
                }

                if (flag || world.getBlockState(new BlockPos(x, y + 1, z)).getBlock().getMaterial() == Material.water)
                {
                    flag = true;
                }

                if (flag){
                   world.setBlockState(new BlockPos(x, y, z), Blocks.ice.getBlockState().getBaseState());
                }
            }
        }
   
    
    public String getName()
	{
	return name;
	}
    
    }